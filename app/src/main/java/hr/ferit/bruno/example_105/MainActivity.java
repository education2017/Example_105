package hr.ferit.bruno.example_105;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.rpvPicker) RangePickerView rpvPicker;
    @BindView(R.id.bShowBounds) Button bShowBounds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bShowBounds)
    public void showBounds(){
        String message = this.rpvPicker.getLeftBound() + "-" + this.rpvPicker.getRightBound();
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
