package hr.ferit.bruno.example_105;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zoric on 3.10.2017..
 */

public class RangePickerView extends RelativeLayout {

    //region BoundFields
    @BindView(R.id.tvLeftBoundText) TextView tvLeftBoundText;
    @BindView(R.id.tvRightBoundText) TextView tvRightBoundText;
    @BindView(R.id.sbLeftBound) SeekBar sbLeftBound;
    @BindView(R.id.sbRightBound) SeekBar sbRightBound;
    //endregion

    //region Constructors

    public RangePickerView(Context context) {
        super(context);
        this.inflate(context);
        this.bind();
    }

    public RangePickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.inflate(context);
        this.bind();
    }

    public RangePickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.inflate(context);
        this.bind();
    }
    //endregion

    //region PrivateMethods
    private void bind() {
        ButterKnife.bind(this);
        this.sbLeftBound.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int leftBound = getLeftBound();
                int rightBound = getRightBound();
                if(leftBound > 99){
                    rightBound = 100;
                    leftBound = 99;
                } else if(rightBound < leftBound){
                    rightBound = leftBound + 1;
                }
                sbLeftBound.setProgress(leftBound);
                sbRightBound.setProgress(rightBound);
                setLeftBoundText();
                setRightBoundText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        this.sbRightBound.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int leftBound = getLeftBound();
                int rightBound = getRightBound();
                if(rightBound < 1){
                    rightBound = 1;
                    leftBound = 0;
                } else if(rightBound < leftBound){
                    leftBound = rightBound - 1;
                }
                sbLeftBound.setProgress(leftBound);
                sbRightBound.setProgress(rightBound);
                setLeftBoundText();
                setRightBoundText();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void inflate(Context context){
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.rangepicker,this,true);
    }
    //endregion

    //region PublicMethods
    public int getLeftBound(){
        return this.sbLeftBound.getProgress();
    }

    public int getRightBound(){
        return this.sbRightBound.getProgress();
    }

    public void setLeftBoundText(){
        this.tvLeftBoundText.setText(String.valueOf(this.getLeftBound()));
    }

    public void setRightBoundText(){
        this.tvRightBoundText.setText(String.valueOf(this.getRightBound()));
    }
    //endregion


}

